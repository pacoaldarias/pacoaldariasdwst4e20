<?php
function recoge($var){
    $tmp = (isset($_POST[$var]))
        ? strip_tags(trim(htmlspecialchars($_POST[$var], ENT_QUOTES, "ISO-8859-1")))
        : "";
    if (get_magic_quotes_gpc()) {
        $tmp = stripslashes($tmp);
    }
    return $tmp;
}
$titulo=recoge("titulo");
$director=recoge("director");
$genero=recoge("genero");
if (($titulo!="")&&($director!="")){	
	print("Titulo: ".$titulo." Director: ".$director." Genero: ".$genero."<br><br>");
	//Guardamos la pelicula en el fichero
	$pelicula = "Titulo: ".$titulo." Director: ".$director." Genero: ".$genero.PHP_EOL;
	$archivo = fopen("datos_peliculas.txt","a");
	if($archivo){
		fwrite($archivo,$pelicula);
		print("Se han guardado los datos correctamente<br><br>");
	}else{
		print("No se han podido guardar los datos<br><br>");
	}
	fclose($archivo);
}else{	
	print("No se han recibido los datos correctamente<br><br>");
}
?>
<a href="pelicula.html">Volver</a><br>